#!/usr/bin/env python

import subprocess
import select
import shutil
import yaml
import sys
import os

action = 'host'
pipeline = None
try:
    if sys.argv[1] == '-c':
        action = 'container'
        pipeline = sys.argv[2]
except IndexError:
    pass

def process_read(p):
    poll = select.epoll()

    poll.register(p.stdout)
    poll.register(p.stderr)

    registered_fd = 2
    while registered_fd > 0:
        ret = poll.poll()
        for fd, events in ret:
            if fd == p.stdout.fileno():
                if events & select.EPOLLIN:
                    line = p.stdout.readline().rstrip()
                    yield (1, line)
                elif events & select.EPOLLHUP:
                    poll.unregister(p.stdout)
                    registered_fd -= 1

            if fd == p.stderr.fileno():
                if events & select.EPOLLIN:
                    line = p.stderr.readline().rstrip()
                    yield (2, line)
                elif events & select.EPOLLHUP:
                    poll.unregister(p.stderr)
                    registered_fd -= 1

    raise StopIteration()


class Pipeline(object):
    def __init__(self, name, scripts, image=None):
        self.name = name
        self.scripts = scripts
        self.image = image

    def run(self, type):
        if type == 'container':
            for cmd in self.scripts:
                self.run_cmd(cmd)
        else:
            tmp = '/tmp/pipeline-%s-%d' % (self.name, os.getpid())

            try:
                cmd = "git clone . %s" % (tmp)
                self.run_cmd(cmd)

                cmd = "sudo docker run --rm " + \
                    "-v %s/pipeline.py:/usr/bin/pipeline " \
                    "-v %s/:/build " \
                    "-v %s/artefacts:/artefacts " \
                    "-w /build " \
                    "builder:centos " \
                    "pipeline -c %s" % (
                        os.path.dirname(sys.argv[0]),
                        tmp, os.getcwd(), self.name
                    )
                self.run_cmd(cmd, split_fds=False)
            except Exception as e:
                raise e
            finally:
                shutil.rmtree(tmp)

    def run_cmd(self, cmd, split_fds=True):
        print "Running %s" % (cmd)
        p = subprocess.Popen(cmd,
                             shell=True,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        for fd, line in process_read(p):
            if split_fds:
                print "%s: %s" % ('O' if fd == 1 else 'E', line)
            else:
                print "%s" % (line)
            sys.stdout.flush()

        p.wait()
        print "Exit code: %d" % (p.returncode)
        if p.returncode != 0:
            sys.exit(p.returncode)


class Pipelines(object):
    def __init__(self):
        self.stages = []

        self.builder = {}
        self.builder_stage = {}

    def run(self):
        global action, pipeline

        if action == 'container':
            self.builder[pipeline].run(action)
        else:
            for s in self.stages:

                for k in self.builder:
                    if self.builder_stage[k] == s:
                        self.builder[k].run(action)


class BBPipelines(Pipelines):
    def __init__(self, default=None):
        super(BBPipelines, self).__init__()
        self.stages = ['default']

        if default:
            scripts = []
            for s in default:
                scripts.extend(s['step']['script'])
            self.builder['default'] = Pipeline('default', scripts)
            self.builder_stage['default'] = 'default'


class GLPipelines(Pipelines):
    def __init__(self, stages, before_script=None, **kargs):
        super(GLPipelines, self).__init__()

        self.stages = stages

        for b in kargs:
            scripts = []
            if before_script:
                scripts.extend(before_script)
            scripts.extend(kargs[b]['script'])
            self.builder[b] = Pipeline(b, scripts)
            self.builder_stage[b] = kargs[b]['stage']


pipelines = [
    {
        'file': "bitbucket-pipelines.yml",
        'class': BBPipelines,
        'resource': 'pipelines',
    },
    {
        'file': ".gitlab-ci.yml",
        'class': GLPipelines,
        'resource': None,
    }
]

for pl in pipelines:
    if os.path.exists(pl['file']):
        with open(pl['file']) as fp:
            config = yaml.load(fp.read())

        if pl['resource']:
            config = config[pl['resource']]

        p = pl['class'](**config)
        p.run()
        break
